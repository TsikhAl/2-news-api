import getNews from './get-news';
import nhlImg from './../imgs/apple-icon-144x144.png';
import cnnImg from './../imgs/apple-touch-icon (1).png';
import nflImg from './../imgs/apple-touch-icon.png';
import mtvImg from './../imgs/apple-touch-icon-precomposed.png';
import cryptoCoinsImg from './../imgs/cropped-ccn-180x180.png';
import espnImg from './../imgs/espn_icon-152x152.min.png';
import natGeogImg from './../imgs/N-120.png';
import timeImg from './../imgs/time-touch_icon_120.png';
import NYTimesImg from './../imgs/apple-touch-icon-319373aaf4524d94d38aa599c56b8655.png';
import engadgetImg from './../imgs/apple-touch-icon-120x120.png';
import tmpl from './../templates/main.html';
import './../less/main.less';

//set img path for icon
let setImgForIcon = (id, data) => {
    let elem = document.getElementById(id);
    elem.setAttribute('src', data);
};

export  default () => {
    let startBtn = document.getElementById('start-btn');

    // shouldn't insert html, if the content already exists
    if (!document.getElementById('news-content')) {
        startBtn.insertAdjacentHTML('afterEnd', tmpl);
    }

    //set imgs for news-icons
    setImgForIcon('nhl', nhlImg);
    setImgForIcon('cnn', cnnImg);
    setImgForIcon('nfl', nflImg);
    setImgForIcon('mtv', mtvImg);
    setImgForIcon('crypto-coins', cryptoCoinsImg);
    setImgForIcon('espn', espnImg);
    setImgForIcon('national-geographic', natGeogImg);
    setImgForIcon('time', timeImg);
    setImgForIcon('the-new-york-times', NYTimesImg);
    setImgForIcon('engadget', engadgetImg);

    let iconsContainer = document.getElementById('news-icons');
    iconsContainer.addEventListener('click', (event) => {

        if (event.target.tagName === 'SPAN' || event.target.tagName === 'IMG' || event.target.tagName === 'LI') {

            let src;
            // for IE 10
            if (window.navigator.appName === 'Microsoft Internet Explorer') {

                src = (event.target.tagName === 'IMG') ? event.target.parentElement.parentElement.attributes[1].textContent :
                    (event.target.tagName === 'SPAN') ? event.target.parentElement.attributes[1].textContent :  event.target.attributes[1].textContent;
            } else {

                src = (event.target.tagName === 'IMG') ? event.target.parentElement.parentElement.dataset.src :
                    (event.target.tagName === 'SPAN') ? event.target.parentElement.dataset.src :  event.target.dataset.src;

            }

            getNews(src);
        }
    });
}
