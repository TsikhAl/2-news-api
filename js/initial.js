let btn = document.getElementById('start-btn');
btn.addEventListener('click', () => {
    // import ('./main').then( (mainJS) => {
    //     mainJS();
    // })

    import(/* webpackChunkName: "mainJS" */ './main').then( module => {
        let mainJS = module.default;
        mainJS();
    })
});
