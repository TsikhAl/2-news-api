export default (data) => {
    if ( !data ) return '';

    let container = document.getElementById('news-content');

    if (container.children.length) {
        container.innerHTML = '';
    }

    for (let i = 0; i < data.articles.length; i++) {

        let author = data.articles[i].author || '',
            title = data.articles[i].title || '',
            description = data.articles[i].description || '',
            publishedAt = data.articles[i].publishedAt || '',
            url = data.articles[i].url || '',
            newsItemContent = `<div class='news-content-item clearfix'><h1>${title}</h1><h2>The author:  ${author}</h2>` +
                            `<p>${description}</p><img src='${data.articles[i].urlToImage}' alt='Sorry, no photo  =('>` +
                            `<span>Published at: ${publishedAt}</span><span>This resource is taken from: <a href='https://newsapi.org/'>News API</a>. If you want more info about this article: <a href='${url}'>CHECK IT</a></span></div>`;

        container.insertAdjacentHTML("beforeEnd", newsItemContent);
    }
};
