import createNewsItem from './create-news-item';

export default (src) => {
    let url = `https://newsapi.org/v2/top-headlines?` +
              `sources=${src}&` +
              `apiKey=d6ef3e11c191461c886ae8204f3af1e9`,
        req = new Request(url);

    fetch(req)
        .then( (response) => {
            let resp = (response.json());
                resp.then( (data) => {
                    createNewsItem(data);
                })
    })
};
