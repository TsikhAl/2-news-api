const path = require('path');
const webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: ['babel-polyfill', 'whatwg-fetch', './js/initial.js'],
    output: {
        path: path.resolve("./dist"),
        filename: "build.js",
        publicPath: "./dist/",
        chunkFilename: "[name].build.js"
    },

    watch: NODE_ENV === 'development',
    watchOptions: {
        aggregateTimeout: 500
    },

    devtool: NODE_ENV === 'development' ? 'source-map' : null,

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.less$/i,
                loader: ExtractTextPlugin.extract(['style-loader','css-loader','less-loader'])
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: {loader: 'html-loader'}
            },
            {
                test: /\.(jpg|png)$/i,
                use: 'file-loader?name=[path][name].[ext]'
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new ExtractTextPlugin('./css/main.css')
    ],

    devServer: {
        inline:true,
        port: 8080
    }
};
